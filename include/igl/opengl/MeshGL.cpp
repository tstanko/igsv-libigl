// This file is part of libigl, a simple c++ geometry processing library.
//
// Copyright (C) 2014 Daniele Panozzo <daniele.panozzo@gmail.com>
//
// This Source Code Form is subject to the terms of the Mozilla Public License
// v. 2.0. If a copy of the MPL was not distributed with this file, You can
// obtain one at http://mozilla.org/MPL/2.0/.

#include "MeshGL.h"
#include "bind_vertex_attrib_array.h"
#include "create_shader_program.h"
#include "destroy_shader_program.h"
#include <iostream>

IGL_INLINE void igl::opengl::MeshGL::init_buffers() {
  // Mesh: Vertex Array Object & Buffer objects
  glGenVertexArrays(1, &vao_mesh);
  glBindVertexArray(vao_mesh);
  glGenBuffers(1, &vbo_V);
  // glGenBuffers(1, &vbo_V_normals);
  // glGenBuffers(1, &vbo_V_ambient);
  glGenBuffers(1, &vbo_V_diffuse);
  // glGenBuffers(1, &vbo_V_specular);
  glGenBuffers(1, &vbo_V_uv);
  glGenBuffers(1, &vbo_V_uv2);
  glGenBuffers(1, &vbo_F);
  glGenTextures(1, &vbo_tex);
  glGenTextures(1, &vbo_tex2);

  // Line overlay
  glGenVertexArrays(1, &vao_overlay_lines);
  glBindVertexArray(vao_overlay_lines);
  glGenBuffers(1, &vbo_lines_F);
  glGenBuffers(1, &vbo_lines_V);
  glGenBuffers(1, &vbo_lines_V_colors);

  // Point overlay
  glGenVertexArrays(1, &vao_overlay_points);
  glBindVertexArray(vao_overlay_points);
  glGenBuffers(1, &vbo_points_F);
  glGenBuffers(1, &vbo_points_V);
  glGenBuffers(1, &vbo_points_V_colors);

  dirty = MeshGL::DIRTY_ALL;
}

IGL_INLINE void igl::opengl::MeshGL::free_buffers() {
  if (is_initialized) {
    glDeleteVertexArrays(1, &vao_mesh);
    glDeleteVertexArrays(1, &vao_overlay_lines);
    glDeleteVertexArrays(1, &vao_overlay_points);

    glDeleteBuffers(1, &vbo_V);
    // glDeleteBuffers(1, &vbo_V_normals);
    // glDeleteBuffers(1, &vbo_V_ambient);
    glDeleteBuffers(1, &vbo_V_diffuse);
    // glDeleteBuffers(1, &vbo_V_specular);
    glDeleteBuffers(1, &vbo_V_uv);
    glDeleteBuffers(1, &vbo_V_uv2);
    glDeleteBuffers(1, &vbo_F);
    glDeleteBuffers(1, &vbo_lines_F);
    glDeleteBuffers(1, &vbo_lines_V);
    glDeleteBuffers(1, &vbo_lines_V_colors);
    glDeleteBuffers(1, &vbo_points_F);
    glDeleteBuffers(1, &vbo_points_V);
    glDeleteBuffers(1, &vbo_points_V_colors);

    glDeleteTextures(1, &vbo_tex);
    glDeleteTextures(1, &vbo_tex2);
  }
}

IGL_INLINE void igl::opengl::MeshGL::bind_mesh() {
  glBindVertexArray(vao_mesh);
  glUseProgram(shader_mesh);
  bind_vertex_attrib_array(shader_mesh, "position", vbo_V, V_vbo, dirty & MeshGL::DIRTY_POSITION);
  bind_vertex_attrib_array(shader_mesh, "color", vbo_V_diffuse, V_diffuse_vbo, dirty & MeshGL::DIRTY_DIFFUSE);
  bind_vertex_attrib_array(shader_mesh, "texcoord", vbo_V_uv, V_uv_vbo, dirty & MeshGL::DIRTY_UV);
  bind_vertex_attrib_array(shader_mesh, "texcoord2", vbo_V_uv2, V_uv2_vbo, dirty & MeshGL::DIRTY_UV);

  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_F);
  if (dirty & MeshGL::DIRTY_FACE)
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned) * F_vbo.size(), F_vbo.data(), GL_DYNAMIC_DRAW);

  glActiveTexture(GL_TEXTURE0);
  glBindTexture(GL_TEXTURE_2D, vbo_tex);
  if (dirty & MeshGL::DIRTY_TEXTURE) {
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    // glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    // glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, tex_u, tex_v, 0, GL_RGBA, GL_UNSIGNED_BYTE, tex.data());
  }
  glUniform1i(glGetUniformLocation(shader_mesh, "tex"), 0);

  glActiveTexture(GL_TEXTURE1);
  glBindTexture(GL_TEXTURE_2D, vbo_tex2);
  if (dirty & MeshGL::DIRTY_TEXTURE) {
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    // glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    // glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, tex2_u, tex2_v, 0, GL_RGBA, GL_UNSIGNED_BYTE, tex2.data());
  }
  glUniform1i(glGetUniformLocation(shader_mesh, "tex2"), 1);
  dirty &= ~MeshGL::DIRTY_MESH;
}

IGL_INLINE void igl::opengl::MeshGL::bind_overlay_lines() {
  bool is_dirty = dirty & MeshGL::DIRTY_OVERLAY_LINES;

  glBindVertexArray(vao_overlay_lines);
  glUseProgram(shader_overlay_lines);
  bind_vertex_attrib_array(shader_overlay_lines, "position", vbo_lines_V, lines_V_vbo, is_dirty);
  bind_vertex_attrib_array(shader_overlay_lines, "color", vbo_lines_V_colors, lines_V_colors_vbo, is_dirty);

  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_lines_F);
  if (is_dirty)
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned) * lines_F_vbo.size(), lines_F_vbo.data(), GL_DYNAMIC_DRAW);

  dirty &= ~MeshGL::DIRTY_OVERLAY_LINES;
}

IGL_INLINE void igl::opengl::MeshGL::bind_overlay_points() {
  bool is_dirty = dirty & MeshGL::DIRTY_OVERLAY_POINTS;

  glBindVertexArray(vao_overlay_points);
  glUseProgram(shader_overlay_points);
  bind_vertex_attrib_array(shader_overlay_points, "position", vbo_points_V, points_V_vbo, is_dirty);
  bind_vertex_attrib_array(shader_overlay_points, "color", vbo_points_V_colors, points_V_colors_vbo, is_dirty);

  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_points_F);
  if (is_dirty)
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned) * points_F_vbo.size(), points_F_vbo.data(), GL_DYNAMIC_DRAW);

  dirty &= ~MeshGL::DIRTY_OVERLAY_POINTS;
}

IGL_INLINE void igl::opengl::MeshGL::draw_mesh(bool solid) {
  glPolygonMode(GL_FRONT_AND_BACK, solid ? GL_FILL : GL_LINE);

  /* Avoid Z-buffer fighting between filled triangles & wireframe lines */
  if (solid) {
    glEnable(GL_POLYGON_OFFSET_FILL);
    glPolygonOffset(1.0, 1.0);
  }
  glDrawElements(GL_TRIANGLES, 3 * F_vbo.rows(), GL_UNSIGNED_INT, 0);

  glDisable(GL_POLYGON_OFFSET_FILL);
  glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
}

IGL_INLINE void igl::opengl::MeshGL::draw_overlay_lines() {
  glDrawElements(GL_LINES, lines_F_vbo.rows(), GL_UNSIGNED_INT, 0);
}

IGL_INLINE void igl::opengl::MeshGL::draw_overlay_points() {
  glDrawElements(GL_POINTS, points_F_vbo.rows(), GL_UNSIGNED_INT, 0);
}

IGL_INLINE void igl::opengl::MeshGL::init() {
  if (is_initialized) { return; }
  is_initialized = true;
  std::string mesh_vertex_shader_string =
      "#version 330\n"
      "uniform mat4 view;"
      "uniform mat4 proj;"
      "in vec3 position;"
      "in vec3 color;"
      "in vec2 texcoord;"
      "in vec2 texcoord2;"
      "out vec3 colori;"
      "out vec2 texcoordi;"
      "out vec2 texcoordi2;"
      "void main()"
      "{"
      "  gl_Position = proj * view * vec4 (position, 1.0);"
      "  texcoordi = texcoord;"
      "  texcoordi2 = texcoord2;"
      "  colori = color;"
      "}";

  std::string mesh_fragment_shader_string =
      "#version 330\n"
      "uniform mat4 view;"
      "uniform mat4 proj;"
      "uniform vec4 fixed_color;"
      "in vec2 texcoordi;"
      "in vec2 texcoordi2;"
      "in vec3 colori;"
      "uniform sampler2D tex;"
      "uniform sampler2D tex2;"
      "uniform float texture_factor;"
      "uniform float texture2_factor;"
      "out vec4 outColor;"
      "void main()"
      "{"
      "  vec4 texColor0 = vec4(colori,1.0);"
      "  vec4 texColor1 = texture(tex, texcoordi);"
      "  vec4 texColor2 = texture(tex2, texcoordi2);"
      "  if (texture_factor==0.0 && texture2_factor==0.0) {"
      "    outColor = fixed_color;"
      "  } else if (texture_factor==0.0) {"
      "    outColor = texColor2;"
      "  } else if (texture2_factor==0.0) {"
      "    if (any(lessThan(texColor0,vec4(1.0))))"
      "      outColor = 0.5*(texColor0+texColor1);"
      "    else"
      "      outColor = texColor1;"
      "  } else if (texColor2.a > 0.0f) {"
      "    outColor = texColor2;"
      "  } else {"
      "    if (any(lessThan(texColor0,vec4(1.0))))"
      "      outColor = 0.5*(texColor0+texColor1);"
      "    else"
      "      outColor = texColor1;"
      "  }"
      "}";

  std::string overlay_vertex_shader_string =
      "#version 330\n"

      "uniform mat4 view;"
      "uniform mat4 proj;"

      "in vec3 position;"
      "in vec3 color;"

      "out vec3 position_world;"
      "out vec3 normal_world;"
      "out vec3 gcolor;"

      "void main()"
      "{"
      " vec3 normal = vec3(0.0,0.0,1.0);"
      "  position_world = position;"
      "  normal_world = normal;"
      "  gcolor = color;"
      "}";

  std::string overlay_geometry_shader_string =
      "#version 330\n"
      "layout (lines) in;"
      "layout (triangle_strip,max_vertices=8) out;"

      "uniform mat4 view;"
      "uniform mat4 proj;"

      "in vec3 position_world[];"
      "in vec3 normal_world[];"
      "in vec3 gcolor[];"

      "uniform float line_thickness;"

      "out vec3 fcolor;"

      "void main(void)"
      "{"
      "    vec3 p0 = position_world[0];"
      "    vec3 p1 = position_world[1];"

      "    vec3 T = normalize( p1 - p0 );"
      "    vec3 N = normalize( normal_world[0] );"
      "    vec3 B = line_thickness*cross( N, T );"

      "    gl_Position = proj * view * vec4(p0 + B,1.0);"
      "    fcolor = gcolor[0];"
      "    EmitVertex();"

      "    gl_Position = proj * view * vec4(p0 - B,1.0);"
      "    fcolor = gcolor[0];"
      "    EmitVertex();"

      "    gl_Position = proj * view * vec4(p1 + B,1.0);"
      "    fcolor = gcolor[1];"
      "    EmitVertex();"

      "    gl_Position = proj * view * vec4(p1 - B,1.0);"
      "    fcolor = gcolor[1];"
      "    EmitVertex();"

      "    EndPrimitive();"
      "}";

  std::string overlay_fragment_shader_string =
      "#version 330\n"
      "in vec3 fcolor;"
      "out vec4 outColor;"
      "void main(void)"
      "{"
      "  outColor = vec4(fcolor,1.0);"
      "}";

  std::string overlay_point_vertex_shader_string =
      "#version 330\n"
      "uniform mat4 view;"
      "uniform mat4 proj;"
      "in vec3 position;"
      "in vec3 color;"
      "out vec3 color_frag;"
      "void main()"
      "{"
      "  gl_Position = proj * view * vec4 (position, 1.0);"
      "  color_frag = color;"
      "}";

  std::string overlay_point_fragment_shader_string =
      "#version 330\n"
      "in vec3 color_frag;"
      "out vec4 outColor;"
      "void main()"
      "{"
      "  if (length(gl_PointCoord - vec2(0.5)) > 0.5) discard;"
      "  outColor = vec4(color_frag, 1.0);"
      "}";

  init_buffers();
  create_shader_program(mesh_vertex_shader_string, mesh_fragment_shader_string, {}, shader_mesh);
  create_shader_program(overlay_geometry_shader_string, overlay_vertex_shader_string, overlay_fragment_shader_string,
                        {}, shader_overlay_lines);
  create_shader_program(overlay_point_vertex_shader_string, overlay_point_fragment_shader_string, {},
                        shader_overlay_points);
}

IGL_INLINE void igl::opengl::MeshGL::free() {
  const auto free = [](GLuint& id) {
    if (id) {
      destroy_shader_program(id);
      id = 0;
    }
  };

  if (is_initialized) {
    free(shader_mesh);
    free(shader_overlay_lines);
    free(shader_overlay_points);
    free_buffers();
  }
}
